const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");
const Subscriber = require("./subscriber");
const userSchema = mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    suscribedAccount: { type: Schema.Types.ObjectId, ref: "Subscriber" },
  },
  { timestamps: true }
);

userSchema.plugin(uniqueValidator);
userSchema.pre("save", function (next) {
  let user = this;
  if (user.suscribedAccount === undefined) {
    Subscriber.findOne({ email: user.email })
      .then((subscriber) => {
        user.suscribedAccount = subscriber;
        next();
      })
      .catch((error) => {
        console.log(`erreur sur l'inscrit : ${error.message}`);
        next(error);
      });
  } else {
    next();
  }
});

module.exports = mongoose.model("User", userSchema);
