const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Subscriber = require("./subscriber");
const courseSchema = mongoose.Schema(
  {
    title: { type: String, required: true, unique: true },
    description: { type: String, required: true },
    items: [],
    courseAccount: { type: Schema.Types.ObjectId, ref: "Suscriber" },
  },
  { timestamps: true }
);

courseSchema.pre("save", function (next) {
  let course = this;
  if (course.courseAccount === undefined) {
    Subscriber.findOne({ email: course.email })
      .then((subscriber) => {
        course.suscribedAccount = subscriber;
        next();
      })
      .catch((error) => {
        console.log(`erreur sur l'inscrit : ${error.message}`);
        next(error);
      });
  } else {
    next();
  }
});

module.exports = mongoose.model("Course", courseSchema);
