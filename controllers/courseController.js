const Course = require("../models/courses");

exports.index = (req, res, next) => {
  Course.find({}, (err, courses) => {
    if (err) next(err);
    res.render("courses/index", { courses: courses });
  });
};

exports.new = (req, res) => {
  res.render("courses/new");
};
exports.postCourses = (req, res, next) => {
  let newCourse = new Course(req.body);

  newCourse.save((err, course) => {
    if (err) next(err);
    res.json(course);
  });
};
