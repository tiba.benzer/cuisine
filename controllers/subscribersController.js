const Subscriber = require("../models/subscriber");

exports.index = (req, res, next) => {
  Subscriber.find({}, (err, subscribers) => {
    if (err) next(err);
    res.render('index',{subscribers: subscribers});
  });
};

exports.new = (req, res) => {
  res.render('subscribe/new')
}
exports.postSubscribers = (req, res, next) => {
  let newSubscriber = new Subscriber(req.body)

  newSubscriber.save((err, subscriber) => {
    if(err) next(err);
    res.json(subscriber)
  })
}
