const User = require("../models/user");

exports.index = (req, res, next) => {
  User.find({}, (err, users) => {
    if (err) next(err);
    res.render("users/index", { users: users });
  });
};

exports.new = (req, res) => {
  res.render("users/new");
};
exports.postUsers = (req, res, next) => {
  let newUser = new User(req.body);

  newUser.save((err, user) => {
    if (err) next(err);
    res.json(user);
  });
};

exports.getUser = (req, res) => {
  let userId = req.params.id;

  User.findById({ _id: userId }, (err, user) => {
    if (err) next(err);
    res.render("users/showUser", { user: user });
  });
};
