const express = require("express");
const router = express.Router();
const userController = require("../controllers/usersController");
// const bcrypt = require("bcrypt");
// const User = require("../models/user");

// router.get("/connexion", function (req, res) {
//   res.render("login");
// });

// router.post("/connexion", function (req, res) {
//   bcrypt
//     .hash(req.body.password, 10)
//     .then((hash) => {
//       const user = new User({
//         email: req.body.email,
//         password: hash,
//       });
//       user.save().then(() => console.log("sauvegarder"));
//     })
//     .catch();
//   res.render("login");
// });

// router.get("/inscription", function (req, res) {
//   res.render("inscription");
// });

// router.post("/inscription", function (req, res) {
//   res.render("inscription");
// });

router.get("/", userController.index);
router.get("/new", userController.new);
router.post("/create", userController.postUsers);
router.get("/:id", userController.getUser);

module.exports = router;
