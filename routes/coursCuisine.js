const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");

router.get("/", courseController.index);
router.get("/new", courseController.new);
router.post("/create", courseController.postCourses);

// router.get("/", function (req, res) {
//   res.render("cours");
// });

// const recipes = [
//   {
//     picture:
//       "https://images.unsplash.com/photo-1569289522127-c0452f372d46?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=629&q=80",

//     ingredients: "biscuits , beurre, fromage, sucre, oeufs, farine, cerises",

//     step1:
//       "Mixez les biscuits pour les réduire en chapelure grossière. Mélangez-y le beurre fondu. Tapissez le fond d’un moule à cake en silicone avec cette pâte à miettes. Réfrigérez pour faire durcir le beurre.",
//     step2:
//       "Préchauffez le four à 140 °C (th. 4-5). Dans une jatte, fouettez le fromage frais avec le sucre et l’arôme de vanille. Dans un bol, fouettez les œufs avec la farine. Incorporez-les à la préparation au fromage frais. Versez dans le moule, sur la pâte raffermie. Enfournez 1 heure. Après refroidissement, réfrigérez 6 heures ou jusqu’au lendemain.",
//     step3:
//       "Rincez, essuyez et équeutez les cerises. Dénoyautez-les. Chauffez 50 g de sucre roux à sec dans une sauteuse jusqu’à ce qu’il caramélise légèrement. Ajoutez les cerises. Faites-les revenir 3 à 4 min sur feu vif. Retirez et mettez-les dans un compotier. Faites bouillir et réduire le jus de cuisson pour le faire épaissir. Versez sur les cerises. Servez-les froides sur le cheesecake démoulé.",
//   },
// ];
// router.get("/recette", function (req, res) {
//   res.render("recipe", { recipes: recipes });
// });

module.exports = router;
