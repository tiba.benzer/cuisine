const express = require("express");
const subscriberRouter = express.Router();

const subscribesController = require("../controllers/subscribersController");

subscriberRouter.get("/", subscribesController.index);
subscriberRouter.get("/new", subscribesController.new);
subscriberRouter.post("/create", subscribesController.postSubscribers);

module.exports = subscriberRouter;