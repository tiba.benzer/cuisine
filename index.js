const express = require("express");
let app = express();
let db = require("./config/db").db;
let mongoose = require("mongoose");

// DATABASE
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("connecte a la base de donnees"))
  .catch((err) => console.error(err));

// VIEW ENGINE SETUP
app.set("views", "./views");
app.set("view engine", "pug");

// BODY PARSER
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// ROUTES SETUP
let home = require("./routes/home");
let coursCuisine = require("./routes/coursCuisine");
let errors = require("./routes/errors");
let users = require("./routes/users");
let subscribers = require("./routes/subscribersRoute");

// app.get("*", function (req, res) {
//   res.render("errors");
// });

app.use("/users/", users);
app.use("/subscribers/", subscribers);
app.use("/cours/", coursCuisine);
app.use("/", home);

// app.get("/users", function (req, res) {
//   res.render("inscription");
// });

// ERREURS

let port = 4000;

app.listen(port, function () {
  console.log("connexion ok !");
});
